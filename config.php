<?php 

function connectDb(){
	if (!($db instanceof PDO))
		$db = new PDO('mysql:host=HOST;dbname=DBNAME;charset=utf8mb4', 'USER', 'PASS');
	return $db;
}
