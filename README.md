# PWA Réservation (cache + traitement formulaires hors ligne)

> Hop vous pouvez installer l'appli sur votre téléphone normalement.  
> Features :  
> * Si vous visitez quelques pages, vous pouvez vous mettre hors ligne et quand même y avoir accès.  
> * Si vous êtes hors ligne et que vous ouvrez l'application, vous aurez des notifications (notifs système + sur la page) qui indiquent que vous êtes offline.  
> * Si vous soumettez le formulaire en étant offline, il est stocké dans une bdd locale sur votre smartphone.  
> * Si vous démarrer l'application et que vous êtes en ligne et que la bdd locale n'est pas vide, le(s) formulaire(s) va (vont) être envoyé(s), et une notification (système + sur la page) vous indiqueront les id de vos réservations.


## Screenshots


### Screenshot audit Lighthouse

![img](img.png)

### Screenshot Audit PWA

![img](img-pwa.png)

### Screenshot étapes utilisation mobile

![mobile](mobile.png)

Étapes:

1. Arrivée sur la page, on remarque une icone (différente selon le navigateur) permettant d'installer l'application web.
2. Le bandeau apparaît après avoir cliqué sur l'icone précédente.
3. La fenêtre apparaît après avoir cliqué sur le bandeau précédent.
4. Une fois l'application installée et ouverte, on remarque qu'il n'y a pas de barre d'adresse ni de bouton d'options (le dernier point dépend du navigateur utilisé). Une image fixe peut également être affichée lors du démarrage de l'application.
5. Formulaire rempli.
6. Si on n'est pas connecté à internet, la notification suivante apparaît si on tente de soumettre un formulaire.
7. La notification est aussi envoyée (elle apparaît donc avec les autres notifications système).
8. Si on est connecté à internet quand on démarre l'application, les formulaires enregistrés en local sont envoyés puis supprimés. Une notification apparaît pour indiquer à l'utilisateur ce qu'il s'est passé.
9. La notification est aussi envoyée (elle apparaît donc avec les autres notifications système).
