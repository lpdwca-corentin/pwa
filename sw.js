self.addEventListener('install', event =>{
	console.log(event);
	caches.open('hotelbookingserviceplus').then(
		cache =>{
			cache.addAll([
				"index.php",
				"script.js",
				"sw.js",
				"design.css",
				"hotel.svg",
				"icon-192.png",
				"icon-512.png",
				"/favicon.ico"
			]);
		}
	);
});

self.addEventListener('activate', event =>{
	console.log(event);
});

self.addEventListener('fetch', event =>{
	console.log(event.request.url);

	event.respondWith(
		caches.match(event.request).then(response=>{

			// response exist in cache
			if(response)
				return response;

			// if post do not save it
			if(event.request.method == "POST")
				return fetch(event.request);

			// is not in cache & not post ? then save it
			else{

				return fetch(event.request).then(
					newResponse=>{
						caches.open('hotelbookingserviceplus').then(
							cache=>cache.put(event.request, newResponse)
						);
						return newResponse.clone();
					}
				);
			}
		})
	);
});
