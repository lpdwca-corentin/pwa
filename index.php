<?php

// db settings
require_once("config.php");

if(isset($_GET['page'])){
	switch ($_GET['page']) {
		case 'submit':
			if(
				isset($_POST['firstname']) &&
				isset($_POST['lastname']) &&
				isset($_POST['childrennb']) &&
				isset($_POST['adultnb']) &&
				isset($_POST['arrival']) &&
				isset($_POST['departure'])
			){
				submit_reservation();
			}
			break;
		case 'view':
			if(isset($_GET['id']))
				view_reservation($_GET['id'], "View reservation &mdash; Hotel booking service plus");
			else
				header('Location: index.php');

			break;
		default:
			homepage("Hotel booking service plus");
			break;
	}
}
else
	homepage("Hotel booking service plus");



function submit_reservation(){
	if(!(
		DateTime::createFromFormat('Y-m-d', $_POST['arrival']) && DateTime::createFromFormat('Y-m-d', $_POST['departure'])
	))
		header('Location index.php');

	$db = connectDb();

	$firstname = htmlspecialchars($_POST['firstname']);
	$lastname = htmlspecialchars($_POST['lastname']);
	$childrennb = (int)$_POST['childrennb'];
	$adultnb = (int)$_POST['adultnb'];
	$arrival = $_POST['arrival'];
	$departure = $_POST['departure'];
	$optionmeal = (isset($_POST['optionmeal']) && $_POST['optionmeal'] == "on") ? 1 : 0;
	$optioncar = (isset($_POST['optioncar']) && $_POST['optioncar'] == "on") ? 1 : 0;

	$req = $db->prepare('INSERT INTO reservations (firstname, lastname, childrennb, adultnb, arrival, departure, optionmeal, optioncar) VALUES (:firstname, :lastname, :childrennb, :adultnb, :arrival, :departure, :optionmeal, :optioncar)');
	$req->execute([
		'firstname' => $firstname,
		'lastname' => $lastname,
		'childrennb' => $childrennb,
		'adultnb' => $adultnb,
		'arrival' => $arrival,
		'departure' => $departure,
		'optionmeal' => $optionmeal,
		'optioncar' => $optioncar
	]);

	print($db->lastInsertId());
	exit(0);
}


function view_reservation($id, $title){

		$id = (int)$id;

		$db = connectDb();

		$req = $db->prepare('SELECT * FROM reservations WHERE id = :id');
		$req->execute([':id' => $id]);
		$reservation = $req->fetch();


		head($title);
		print_reservation($reservation);
}



function print_reservation($reservation){
	?>
	<p>Reservation <span class="number">#<?php print($reservation['id']); ?></span> of <b><?php print($reservation['firstname'] ." ". $reservation['lastname']); ?></b>.</p>

	<ul>
		<li>Number of childs : <?php print($reservation['childrennb']); ?></li>
		<li>Number of adults : <?php print($reservation['adultnb']); ?></li>
		<li>Arrival : <?php print($reservation['arrival']); ?></li>
		<li>Departure : <?php print($reservation['departure']); ?></li>
		<li>Options :
			<ul>
				<li>Meal included : <?php print($reservation['optionmeal'] ? "yes" : "no"); ?></li>
				<li>Rent-a-car : <?php print($reservation['optioncar'] ? "yes" : "no"); ?></li>
			</ul>
		</li>
	</ul>

	<?php
}


function head($title){
	?>
	<!DOCTYPE html>
	<html lang="en">
		<meta charset="utf-8" />
		<title><?php print($title); ?></title>
		<meta name="viewport" content="width=device-width" />
		<meta name="theme-color" content="#efe" />
		<meta name="description" content="Hotel booking service plus is an example of optimized PWA which will cache your form content if you are offline, and will then send it to the server." />
		<link rel="stylesheet" href="design.css" />
		<link rel="manifest" href="manifest.webmanifest" />
		<link rel="apple-touch-icon" href="icon-192.png" />
		<link rel="preload" href="design.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
		<style type="text/css">
			html{scroll-behavior:smooth}body,html{padding:0;width:100%;max-width:800px;margin:0 auto;height:100%;box-sizing:border-box}body{font-size:1.1em;font-family:sans-serif;background-color:#efe}h1{font-size:1.3em;text-align:center;font-weight:400}p{color:#343;text-align:center}p,section{padding:5px 3px;background-color:#fff;box-shadow:0 0 3px 2px #fff}section{padding-bottom:10px;margin-bottom:10px}aside,aside img{margin:auto;max-width:80%;text-align:center}input,label{width:80%;display:block;margin:auto;padding:10px}label{font-size:.8em;margin-top:10px}input[type=number]{width:60%}input[type=checkbox]{display:none}label.checkbox{width:80%;font-size:1.1em}label.checkbox::after{color:#822;width:100%;content:"no";text-align:right;display:inline-block;transition:color .1s}label.checkbox::before{font-family:monospace;content:"[ ] "}input[type=button],input[type=submit]{border:none;margin:5px auto;background-color:#ccc;box-shadow:0 3px #aaa inset;transition:box-shadow .2s}#notifZone{background-color:#fff;overflow:hidden;font-size:1.3em;padding:0 10px;max-height:0}@media all and (min-width:850px){section{display:flex;flex-direction:row;align-items:center;justify-content:space-between}aside{max-width:20vw}article{flex-grow:1}}
		</style>
	</head>
	<body>
		<div id="notifZone"></div>
		<h1><a href="index.php">Never miss your chance</a></h1>
	<?php
}


function homepage($title){
	head($title);
	?>
		<p>We'll help you to find the best place for you.</p>

		<section>
			<aside>
				<img src="hotel.svg" alt="hotel logo" />
			</aside>
			<article>
				<p>
					Please fill this form to book an hotel.
				</p>
				<form method="post" action="index.php?page=submit" id="formBook">

					<label for="firstname">First name</label>
					<input type="text" name="firstname" id="firstname" />

					<label for="lastname">Last name</label>
					<input type="text" name="lastname" id="lastname" />

					<label for="childrennb">Children nb</label>
					<input type="number" name="childrennb" id="childrennb" min="0" max="40" />

					<label for="adultnb">Adult nb</label>
					<input type="number" name="adultnb" id="adultnb" min="1" max="40" />


					<label for="arrival">Arrival</label>
					<input type="date" name="arrival" id="arrival" />

					<label for="departure">Departure</label>
					<input type="date" name="departure" id="departure" />

					<input type="checkbox" id="optionmeal" name="optionmeal" />
					<label for="optionmeal" class="checkbox">Meal included</label>

					<input type="checkbox" id="optioncar" name="optioncar" />
					<label for="optioncar" class="checkbox">Rent-a-car</label>

					<input type="button" value="Send" id="submitBook" />
				</form>
			</article>
		</section>

		<section>
			<article>
				<h1>View my reservation</h1>
				<form method="get" action="index.php">
					<label for="id">Please enter your id here:</label>
					<input type="number" name="id" id="id" />
					<input type="hidden" name="page" value="view" />
					<input type="button" value="See my reservation" id="submitView" />
				</form>
			</article>
		</section>

		<script src="script.js"></script>
		<script src="sw.js"></script>
	</body>
	</html>
	<?php
	}
?>
