// register service worker
if('serviceWorker' in navigator)
	navigator.serviceWorker.register('sw.js');

// notify if offline?
if(!window.navigator.onLine)
	showNotification("Well then it looks like you're offline.");
// else look for localDb data and send it if found
else{
	// open db
	var request = window.indexedDB.open("Hotelbookinglocaldb", 3);

	// if the db doesn't exist of it was updated this function will be called
	request.onupgradeneeded = function(event) {
		var db = event.target.result;
		var store = db.createObjectStore("reservation", { autoIncrement : true });
	}

	request.onsuccess = function(event) {

		// get db connexion
		var db = event.target.result;

		// get to our table
		var store = db.transaction(["reservation"], "readwrite");
		var store = store.objectStore("reservation");

		// search for offline bookings that have not already been sent
		store.openCursor().onsuccess = function(event) {
			var cursor = event.target.result;
			if (cursor) {
				formData = JSON.parse(cursor.value);

				var form = new FormData();

				for (let [key, value] of Object.entries(formData)) {
					console.log(key +" - " + value);
					form.append(key, value);
				}

				fetch("index.php?page=submit", {
					method: "POST",
					body: form
				}).then(
					response => response.text()
				).then(
					// then redirect user to the "view" page, so he can see the informations
					id => {
						showNotification("Your form was successfully sent, you can view it using id : " + id + ".");
						console.log("index.php?id=" + id + "&page=view");
					}
				);

				cursor.delete();
				cursor.continue();
			}
			else {
				console.log("no more entries!");
			}
		};
	}
}

// handle submission of new reservations
document.getElementById('submitBook').addEventListener("click", function () {

	var form = new FormData(document.getElementById('formBook'));


	// if we're online, just send the form content
	if(window.navigator.onLine){
		fetch("index.php?page=submit", {
			method: "POST",
			body: form
		}).then(
			response => response.text()
		).then(
			// then redirect user to the "view" page, so he can see the informations
			id => {
				window.location.href = "index.php?id=" + id + "&page=view";
			}
		);
	}
	else{
		// if we're offline, create a database (or open it)
		var request = window.indexedDB.open("Hotelbookinglocaldb", 3);

		// if user refused db storage, let him know that we're not happy :c
		request.onerror = function(event) {
			showNotification("Failed to save your booking order on your device.");
		};

		// if the db doesn't exist of it was updated this function will be called
		request.onupgradeneeded = function(event) {
			var db = event.target.result;
			var store = db.createObjectStore("reservation", { autoIncrement : true });
		}

		// if we're connected to db, store form inside it and notify the user
		request.onsuccess = function(event) {

			// get connexion
			var db = event.target.result;
			// get table "reservation" with write access
			var transaction = db.transaction(["reservation"], "readwrite");

			// event handler when save is successful
			transaction.oncomplete = function(event) {
				showNotification("We successfully saved your reservation. Please connect your device to Internet in order to send the data.");
			};

			// event handler when there was a problem (not enough space?)
			transaction.onerror = function(event) {
				showNotification("Failed to save your booking order on your device.");
			};

			// store a new "reservation" object
			var objectStore = transaction.objectStore("reservation");

			// we can't store Formdata inside db, so convert it to json first
			let jsonForm = JSON.stringify(Object.fromEntries(form));

			// send data in db
			var savedData = objectStore.add(jsonForm);
		};
	}
});

// handle view of reservations
document.getElementById('submitView').addEventListener("click", function () {
	console.log("book");
	window.location.href = "index.php?id=" + document.getElementById("id").value + "&page=view";
});


// show notification
function showNotification(text) {
	Notification.requestPermission().then(function(result) {
		if (result === 'granted') {
			navigator.serviceWorker.ready.then(function(registration) {
				registration.showNotification('Hotel booking service plus', {
					body: text,
					icon: 'icon-192.png'
				});
			});
		}
	});

	let notif = document.getElementById("notifZone");
	notif.innerHTML = text;
	showNotificationOnPage();
}


function showNotificationOnPage(){
	document.getElementById("notifZone").classList.add("animated");
	setTimeout(function() {
		document.getElementById("notifZone").classList.remove("animated");
	}, (6000));
}